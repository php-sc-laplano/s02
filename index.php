<?php require_once"./code.php"?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S02 Repetition and Control Structures and Array Manipulation A1</title>
</head>
<body>
	<h2>Divisible by 5</h2>
	<?php isDivisibleBy5()?>

	<h3>Add in array</h3>

	<?php array_push($students, 'John Smith')?>
	<pre><?php print_r($students)?></pre>
	<pre><?php echo count($students)?></pre>
	<?php array_push($students, 'Jane Smith')?>
	<pre><?php print_r($students)?></pre>
	<pre><?php echo count($students)?></pre>

	<?php array_shift($students)?>
	<pre><?php print_r($students)?></pre>

	<pre><?php echo count($students)?></pre>
</body>
</html>